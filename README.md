# Away3D Automatic Doors Showcase

A 3D application written using Away3D and Flash to showcase automatic doors.

This project was abandoned a long time ago though.

[A working demo can be found here](https://nicomv.com/flash/away3d-automatic-doors).

## How To Run

I have tested it only on Windows 7, but it should work for Windows XP and up:

1. Install the 32 bit Java Runtime Environment (even if using a 64bit OS) and setup the __JAVA\_HOME__ environment variable to point to it.
2. Install .NET 3.5.
3. [Install FlashDevelop] (http://www.flashdevelop.org/downloads/releases/FlashDevelop-5.3.3.exe).
4. [Download Adobe Flex from (click here)](https://www.adobe.com/devnet/flex/flex-sdk-download.html).
5. Setup the project to point to the location of the Adobe Flex SDK.
6. Lastly, import/open the project on FlashDevelop and press F5.

If something is not working, be sure to let Me know :).

## License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
