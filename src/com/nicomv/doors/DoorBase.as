package com.nicomv.doors {
	import adobe.utils.CustomActions;
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.entities.Mesh;
	import away3d.library.assets.IAsset;
	import away3d.materials.MaterialBase;
	import away3d.materials.ColorMaterial;
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.tools.commands.Merge;
	import away3d.events.AssetEvent;
	import away3d.events.LoaderEvent;
	import away3d.library.AssetLibrary;
	import away3d.loaders.misc.AssetLoaderToken;
	import away3d.loaders.parsers.Max3DSParser;
	import away3d.loaders.parsers.OBJParser;
	import away3d.tools.utils.Bounds;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import away3d.materials.TextureMaterial;
	
	import flash.net.URLRequest;
	
	import caurina.transitions.Tweener;
	
	import flash.geom.Vector3D;
	
	import com.nicomv.Utils;
	
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class DoorBase extends EventDispatcher {
		public static const	DONE: String			= 'done';
		public static const OPEN_COMPLETE: String	= 'openComplete';
		public static const BLACK: uint				= 0x000000;
		
		protected var _scene: Scene3D				= null;
		protected var _name: String					= 'BaseDoor';
		protected var _modelName: String 			= '';
		
		protected var _scaleX: Number				= 0;
		protected var _scaleY: Number				= 0;
		protected var _scaleZ: Number				= 0;
		
		protected var _color: Object				= null;
		
		protected var _doors: ObjectContainer3D		= null;
		protected var _picker: StaticLightPicker	= null;
		
		protected var _doorsOpen: Boolean			= false;
		protected var _modelLoaded: Boolean			= false;
		
		// Frame:
		protected var _finalFrame: Mesh				= null;
		protected var _frame: Mesh					= null;
		protected var _windows: Array				= null;
		
		protected var _merge: Merge					= null;
		protected var _materials: Object			= null;
		
		public function DoorBase( scene: Scene3D, picker: StaticLightPicker, scale: Array, color: Object ) {
			_scene		= scene;
			_picker		= picker;
			
			if ( scale === null )
				scale = new Array( 1, 1, 1 );
			
			_scaleX		= scale[0];
			_scaleY		= scale[1];
			_scaleZ		= scale[2];
			_color		= color;
			_doors		= new ObjectContainer3D();
			_merge		= new Merge( true );
			
			_materials	= new Object();
			
			AssetLibrary.enableParser( Max3DSParser );
			AssetLibrary.enableParser( OBJParser );
		}
		
		public function init(): void {
			trace( 'DoorBase.init' );
		}
		
		protected function loadModel( cback: Function = null ): void {
			trace( 'Loading model...' );
			var modelFile: String		= 'assets/models/doors/' + _modelName;
			var request: URLRequest		= new URLRequest( modelFile );
			var token: AssetLoaderToken	= AssetLibrary.load( request );
			token.addEventListener( AssetEvent.ASSET_COMPLETE, assetComplete );
			token.addEventListener( AssetEvent.MESH_COMPLETE, meshComplete );
			token.addEventListener( LoaderEvent.RESOURCE_COMPLETE, modelLoaded );
			token.addEventListener( LoaderEvent.LOAD_ERROR, modelLoadingError );
			token.addEventListener( AssetEvent.MATERIAL_COMPLETE, materialLoaded );
		}
		
		protected function modelLoaded( e: LoaderEvent = null, cback: Function = null ): void {
			_modelLoaded = true;
		}
		
		protected function addWindows( door: ObjectContainer3D ): void {
			trace( 'Adding windows to ' + door.name );
		}
		
		protected function meshComplete( e: AssetEvent ): void {
			var mesh: Mesh		= Mesh( e.asset );
			
			switch( mesh.name ) {
				case 'Frame_frame':
					_frame				= mesh;
					_frame.mouseEnabled	= true;
					break;
			}
		}
		
		protected function modelLoadingError( e: LoaderEvent ): void {
			trace( 'An error ocurred loading ' + e.url + ': ' + e.message );
		}
		
		protected function assetComplete( e: AssetEvent ): void {
			trace( 'Asset complete: ' + [e.asset, e.asset.name] );
		}
		
		protected function materialLoaded( e: AssetEvent ): void {
			trace( 'Material ' + e.asset.name + ' loaded!' );
			
			if ( typeof _materials[e.asset.name] == 'undefined' ) {
				if ( e.asset is ColorMaterial ) {
					var mat: ColorMaterial	= ColorMaterial( e.asset );
					_materials[e.asset.name]	= mat;
				} else {
					_materials[e.asset.name]	= e.asset;
				}
				
				_materials[e.asset.name].lightPicker	= _picker;
			}
		}
		
		public function toggleDoor( lcback: Function = null, rcback: Function = null ): void {
			trace( 'Opening door... ' );
		}
		
		public function emergencyToggleDoors( lastScale: Object = null, lcback: Function = null, rcback: Function = null ): void {
			trace( 'DoorBase.emergencyOpenDoors()' );
		}
		
		public function get name(): String {
			return _name;
		}
		
		public function get modelName(): String {
			return _modelName;
		}
		
		public function set scaleX( sx: Number ): void {
			_scaleX = sx;
		}
		
		public function set scaleY( sy: Number ): void {
			_scaleY 		= sy;
		}
		
		public function set scaleZ( sz: Number ): void {
			_scaleZ 		= sz;
		}
	
		public function get scaleX(): Number {
			return _scaleX;
		}
		
		public function get scaleY(): Number {
			return _scaleY;
		}
		
		public function get scaleZ(): Number {
			return _scaleZ;
		}
		
		public function scaleTo( x: Number, y: Number, z: Number ): void {
			_scaleX	= x;
			_scaleY	= y;
			_scaleZ	= z;
			
			_doors.scaleX = _scaleX;
			_doors.scaleY = _scaleY;
			_doors.scaleZ = _scaleZ;
			
			_finalFrame.scaleX = _scaleX;
			_finalFrame.scaleY = _scaleY;
			_finalFrame.scaleZ = _scaleZ;
			
			dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		public function get doorWidth(): Number {
			var tmp: Mesh = Mesh( _doors.getChildAt(0) );
			
			return ( tmp.maxX - tmp.minX );
		}
		
		public function get open(): Boolean {
			return _doorsOpen;
		}
		
		public function get doors(): ObjectContainer3D {
			return _doors;
		}
		
		public function getDoor( name: String ): Mesh {
			return Utils.getChildByName( _doors, name );
		}
	
		public function changeColor( color: Object ): void {
			_color						= color;
			var material: ColorMaterial	= ColorMaterial( Mesh( _doors.getChildAt(0).getChildAt(0) ).material );
			material					= applyPropsToMaterial( material );
			
			for ( var i: int = 0; i < _doors.numChildren; i++ ) {
				var mesh: Mesh	= Mesh( _doors.getChildAt(i).getChildAt(0) );
				mesh.material	= material;
			}
		}
		
		protected function applyPropsToMaterial( mat: * ): * {
			if( _color.ambient !== undefined )
				mat.ambient			= _color.ambient;
				
			if( _color.ambientColor !== undefined )
				mat.ambientColor	= _color.ambientColor;
				
			if( _color.color !== undefined )
				mat.color			= _color.color;
				
			if( _color.specular !== undefined )
				mat.specular		= _color.specular;
				
			if( _color.specularColor !== undefined )
				mat.specularColor	= _color.specularColor;
				
			return mat;
		}
		
		protected function resetPivotPoints(): void {
			for ( var i: int = 0; i < _doors.numChildren; i++ ) {
				_doors.getChildAt(i).pivotPoint = new Vector3D( 0, 0, 0 );
			}
		}
		
		protected function setupFrame(): void {}
		
		public function get frame(): Mesh {
			return _finalFrame;
		}
		
		public function get frameWidth(): Number {
			return (_frame.maxX - _frame.minX);
		}
		
		public function get frameHeight(): Number {
			return (_frame.maxY - _frame.minY);
		}
		
		public function dispose(): void {
			_doors.dispose();
			_frame.dispose();
			_finalFrame.dispose();
		}
	}
}