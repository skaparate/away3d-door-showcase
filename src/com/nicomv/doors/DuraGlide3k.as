package com.nicomv.doors {
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.core.math.Vector3DUtils;
	import away3d.entities.Mesh;
	import away3d.entities.SegmentSet;
	import away3d.events.LoaderEvent;
	import away3d.materials.ColorMaterial;
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.primitives.LineSegment;
	import caurina.transitions.Tweener;
	import com.nicomv.Utils;
	import flash.events.Event;
	import flash.geom.Vector3D;
	import away3d.events.AssetEvent;
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class DuraGlide3k extends DoorBase {
		private static var _lastScale: Object	= new Object();
		
		protected var _frontSensor: Mesh			= null;
		protected var _backSensor: Mesh				= null;
		protected var _sensors: Vector.<Mesh>		= null;
		
		public function DuraGlide3k( scene: Scene3D, picker: StaticLightPicker, scale: Array, color: Object ) {
			super( scene, picker, scale, color );
			_name		= 'Dura Glide 3000';
			_modelName	= 'DuraGlide3k.obj';
			_windows	= new Array(
				[ null, null ],
				[ null, null ],
				[ null, null ],
				[ null, null ]
			);
		}
		
		override public function init(): void {
			super.init();
			
			if( ! _modelLoaded )
				loadModel();
			else
				dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		override protected function modelLoaded( e: LoaderEvent = null, cback: Function = null ): void {
			setupFrame();
			addWindows( _doors.getChildAt(0) );
			addWindows( _doors.getChildAt(1) );
			addWindows( _doors.getChildAt(2) );
			addWindows( _doors.getChildAt(3) );
			changeColor( _color );
			scaleTo( _scaleX, _scaleY, _scaleZ );
			_modelLoaded = true;
			
			dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		override protected function addWindows( door: ObjectContainer3D ): void {
			switch( door.name ) {
				case 'doorFL':
					door.addChildren( _windows[0][0], _windows[0][1] );
					break;
					
				case 'doorBL':
					door.addChildren( _windows[1][0], _windows[1][1] );
					break;
					
				case 'doorBR':
					door.addChildren( _windows[2][0], _windows[2][1] );
					break;
					
				case 'doorFR':
					door.addChildren( _windows[3][0], _windows[3][1] );
					break;
			}
		}
		
		override protected function meshComplete( e: AssetEvent ): void {
			super.meshComplete( e );
			var mesh: Mesh	= Mesh( e.asset );
			
			if ( /\w{3}Window/.test( mesh.name ) ) {
				switch( mesh.name ) {
					case 'FLTWindow_topWindow':
						_windows[0][0] = mesh;
						break;
						
					case 'FLBWindow_bottomWindow':
						_windows[0][1] = mesh;
						break;
						
					case 'BLTWindow_topWindow':
						_windows[1][0] = mesh;
						break;
						
					case 'BLBWindow_bottomWindow':
						_windows[1][1] = mesh;
						break;
					
					case 'BRTWindow_topWindow':
						_windows[2][0] = mesh;
						break;
						
					case 'BRBWindow_bottomWindow':
						_windows[2][1] = mesh;
						break;
						
					case 'FRTWindow_topWindow':
						_windows[3][0] = mesh;
						break;
						
					case 'FRBWindow_bottomWindow':
						_windows[3][1] = mesh;
						break;
				}
			} else {
				var cont: ObjectContainer3D 	= new ObjectContainer3D();
				
				switch( mesh.name ) {
					case 'DoorFL_door':
						mesh.name = cont.name = 'doorFL';
						cont.addChild( mesh );
						_doors.addChild( cont );
						break;
					
					case 'DoorBL_door':
						mesh.name = cont.name = 'doorBL';
						cont.addChild( mesh );
						_doors.addChild( cont );
						break;
						
					case 'DoorBR_door':
						mesh.name = cont.name = 'doorBR';
						cont.addChild( mesh );
						_doors.addChild( cont );
						break;
						
					case 'DoorFR_door':
						mesh.name = cont.name = 'doorFR';
						cont.addChild( mesh );
						_doors.addChild( cont );
						break;
						
					case 'FrontSensor_sensor':
						_frontSensor	= mesh;
						break;
					
					case 'BackSensor_sensor':
						_backSensor		= mesh;
						break;
				}
			}
		}
		
		override protected function setupFrame(): void {
			trace( 'Setup Frame' );
			super.setupFrame();
			_sensors					= new <Mesh>[ _frontSensor, _backSensor ];
			_finalFrame					= _merge.applyToMeshes( _frame, _sensors );
			_finalFrame.mouseEnabled	= true;
		}
		
		override public function changeColor( color: Object ): void {
			super.changeColor( color );
			changeFrameColor();
			_finalFrame			 		= _merge.applyToMeshes( _frame, _sensors );
		}
		
		private function changeFrameColor(): void {
			var material: ColorMaterial = ColorMaterial( _frame.material );
			material					= applyPropsToMaterial( material );
			_frame.material				= material;
		}
		
		override public function toggleDoor( lcback: Function = null, rcback: Function = null ): void {
			trace( 'ToggleDoor() ---' );
			var doorBL: ObjectContainer3D	= Utils.getChildByName( _doors, 'doorBL' ),
				doorBR: ObjectContainer3D	= Utils.getChildByName( _doors, 'doorBR' );
				
			if ( ! Tweener.isTweening( doorBL ) && ! Tweener.isTweening( doorBR ) ) {
				trace( 'No tween active. DoorsOpen: ' + _doorsOpen );
				var width: Number			= (doorBL.maxX - doorBL.minX) * 0.93;
				var params: Object	= {
					transition: 'linear',
					time: 0.4,
					x: 0
				};
				
				if ( _doorsOpen ) { // Slide close the doors
					trace( 'Slide closing doors' );
					params.x	= doorBL.x + width;
					
					if ( lcback !== null )
						params.onComplete = function(): void {
							lcback( doorBL );
						}

					Tweener.addTween( doorBL, params );
					
					params.x	= doorBR.x - width;
					params.onComplete = function(): void {
						_doorsOpen = false;
						
						if( rcback !== null )
							rcback( doorBR );
					}
						
					Tweener.addTween( doorBR, params );
				} else { // Slide open the doors
					trace( 'Slide opening doors' );
					params.x	= doorBL.x - width;
					
					if ( lcback !== null ) {
						params.onComplete = function(): void {
							lcback( doorBL );
						}
					}
					
					Tweener.addTween( doorBL, params );
					
					params.x 			= doorBR.x + width;
					params.onComplete	= function(): void {
						_doorsOpen	= true;
						
						if( rcback !== null )
							rcback( doorBR );
					}
					
					Tweener.addTween( doorBR, params );
				}
			}
		}
	
		override public function emergencyToggleDoors( lastScale: Object = null, lcback: Function = null, rcback: Function = null ): void {
			var doorfl: ObjectContainer3D	= Utils.getChildByName( _doors, 'doorFL' ),
				doorfr: ObjectContainer3D	= Utils.getChildByName( _doors, 'doorFR' ),
				width: Number				= doorfl.maxX - doorfl.minX;
				
			if ( ! Tweener.isTweening( doorfl ) && ! Tweener.isTweening( doorbl ) ) {
				var params: Object = {
					transition: 'linear',
					time: 0.3
				};
				
				if ( _doorsOpen ) {
					var doorbl: ObjectContainer3D	= Utils.getChildByName( _doors, 'doorBL' ),
						doorbr: ObjectContainer3D	= Utils.getChildByName( _doors, 'doorBR' );
						
					trace( 'CLOSING Emergency' );
					params.rotationY	= 0;
					params.z			= 0;
					
					// LEFT DOORS:
					Tweener.addTween( doorfl, params );
					Tweener.addTween( doorbl, params );
					
					// RIGHT DOORS:
					Tweener.addTween( doorfr, params );
					
					params.onComplete	= function(): void {
						trace( 'Finished pivot...' );
						resetPivotPoints();
						Tweener.removeAllTweens();
						toggleDoor( lcback, rcback );
					}
					
					Tweener.addTween( doorbr, params );
				} else {
					trace( 'OPENING Emergency' );
					params.z				= -1.5;
					toggleDoor( function( door: ObjectContainer3D ): void {
						doorfl.pivotPoint	= new Vector3D( -((width * 2 ) * 0.92), 0, 0 );
						params.rotationY	= 90;
						Tweener.addTween( doorfl, params );
						
						params.onComplete	= function(): void  {
							if ( lcback !== null )
								lcback();
						}
						
						door.pivotPoint = new Vector3D( -(width * 0.835), 0, 0 );
						Tweener.addTween( door, params );
					}, function( door: ObjectContainer3D ): void {
						params.rotationY	= -90;
						
						doorfr.pivotPoint	= new Vector3D( ((width * 2 ) * 0.92), 0, 0 );
						Tweener.addTween( doorfr, params );
						
						params.onComplete	= function(): void {
							_doorsOpen = true;
							if ( rcback !== null )
								rcback();
						}
						
						door.pivotPoint		= new Vector3D( (width * 0.835), 0, 0 );
						Tweener.addTween( door, params );
					} );
				}
			}
		}
	
		override public function dispose(): void {
			super.dispose();
			_frontSensor.dispose();
			_backSensor.dispose();
		}
	}

}