package com.nicomv.doors {
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.entities.Mesh;
	import away3d.events.AssetEvent;
	import away3d.events.LoaderEvent;
	import away3d.materials.ColorMaterial;
	import away3d.materials.lightpickers.StaticLightPicker;
	import com.nicomv.Utils;
	import flash.events.Event;
	import caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class DuraGlideSLH extends DoorBase {
		private var _header: Mesh			= null;
		private var _rails: Object			= null;
		
		public function DuraGlideSLH( scene: Scene3D, picker: StaticLightPicker, scale: Array, color: Object ) {
			super( scene, picker, scale, color );
			
			_name		= 'Dura Glide SLH';
			_modelName	= 'DuraGlideSLH.obj';
			_windows	= new Array( null, null );
			_rails		= new Object();
		}
		
		override public function init(): void {
			super.init();
			
			if( ! _modelLoaded )
				loadModel();
			else
				dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		override protected function modelLoaded( e: LoaderEvent = null, cback: Function = null ): void {
			super.modelLoaded( e, cback );
			setupFrame();
			addWindows( _doors.getChildAt(0) );
			addWindows( _doors.getChildAt(1) );
			changeColor( _color );
			scaleTo( _scaleX, _scaleY, _scaleZ );
			_modelLoaded = true;
			dispatchEvent( new Event( DoorBase.DONE ) );
		}
		
		override protected function setupFrame(): void {
			trace( 'Setup Frame' );
			super.setupFrame();
			var cont: ObjectContainer3D	= new ObjectContainer3D();
			cont.addChildren( _frame, _rails.left, _rails.right );
			_finalFrame					= _merge.applyToContainer( cont );
			_finalFrame.mouseEnabled	= true;
		}
		
		override protected function addWindows( door: ObjectContainer3D ): void {
			trace( 'Adding windows to ' + door.name );
			switch( door.name ) {
				case 'leftDoor':
					door.addChild( _windows[0] );
					break;
					
				case 'rightDoor':
					door.addChild( _windows[1] );
					break;
			}
		}
		
		override protected function meshComplete( e: AssetEvent ): void {
			var mesh: Mesh	= Mesh( e.asset );
			
			if ( /\w+Glass/.test( mesh.name ) ) {
				switch( mesh.name ) {
					case 'LDGlass_doorGlass':
						_windows[0]	= mesh;
						break;
						
					case 'RDGlass_doorGlass':
						_windows[1]	= mesh;
						break;
				}
			} else {
				var cont: ObjectContainer3D	= new ObjectContainer3D();
				
				switch( mesh.name ) {
					case 'LDSlider_doorSlider':
						mesh.name	= cont.name	= 'leftDoor';
						cont.addChild( mesh );
						_doors.addChild( cont );
						break;
						
					case 'RDSlider_doorSlider':
						mesh.name	= cont.name	= 'rightDoor';
						cont.addChild( mesh );
						_doors.addChild( cont );
						break;
					
					case 'Header_header':
						_frame		= mesh;
						break;
						
					case 'Frame_frame':
						_frame		= mesh;
						break;
						
					case 'RRail_bottomRail':
						_rails.right = mesh;
						break;
						
					case 'LRail_bottomRail':
						_rails.left	 = mesh;
						break;
				}
			}
		}
		
		override public function toggleDoor( left: Function = null, right: Function = null ): void {
			var ldoor: ObjectContainer3D	= Utils.getChildByName( _doors, 'leftDoor' ),
				rdoor: ObjectContainer3D	= Utils.getChildByName( _doors, 'rightDoor' ),
				width: Number				= (ldoor.maxX - ldoor.minX) * 0.87;
				
			if ( ! Tweener.isTweening( ldoor ) && ! Tweener.isTweening( rdoor ) ) {
				var params: Object	= {
					transition: 'linear',
					time: 0.3,
					x: 0
				};
				
				if ( _doorsOpen ) {
					params.x	= ldoor.x + width;
					
					if ( left !== null ) {
						params.onComplete	= function(): void {
							left();
						}
					}
					
					Tweener.addTween( ldoor, params );
					
					params.x	= rdoor.x - width;
					params.onComplete	= function(): void {
						_doorsOpen	= false;
						
						if ( right !== null ) {
							right();
						}
					}
						
					Tweener.addTween( rdoor, params );
				} else {
					params.x	= ldoor.x - width;
					
					if ( left !== null ) {
						params.onComplete	= function(): void {
							left();
						}
					}
					
					Tweener.addTween( ldoor, params );
					
					params.x	= rdoor.x + width;
					params.onComplete	= function(): void {
						_doorsOpen	= true;
						
						if ( right !== null ) {
							right();
						}
					}
						
					Tweener.addTween( rdoor, params );
				}
			}
		}
		
		//public function get header(): Mesh {
			//return _header;
		//}
		
		override public function changeColor( color: Object ): void {
			super.changeColor( color );
			var mat: ColorMaterial	= ColorMaterial( _frame.material );
			mat						= applyPropsToMaterial( mat );
			_frame.material		= mat;
			
			var vec: Vector.<Mesh>	= new <Mesh>[ _rails.left, _rails.right ];
			_finalFrame	= _merge.applyToMeshes( _frame, vec );
		}
		
		override public function dispose(): void {
			super.dispose();
			//_frame.dispose();
		}
	}

}