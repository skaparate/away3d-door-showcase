﻿package com.nicomv.engine {
	
	import adobe.utils.CustomActions;
	import away3d.containers.Scene3D;
	import away3d.containers.View3D;
	import away3d.controllers.HoverController;
	import away3d.core.partition.CameraNode;
	import com.nicomv.engine.Input;
	import flash.events.MouseEvent;
	import flash.geom.Vector3D;
	
	import flash.display.Sprite;
	import flash.ui.Keyboard;
	
	import away3d.cameras.Camera3D;
	import away3d.containers.ObjectContainer3D;
	import away3d.controllers.LookAtController;
	import away3d.cameras.lenses.PerspectiveLens;
	import away3d.cameras.lenses.OrthographicLens;
	
	public class Camera extends Input {
		private var _debug: Boolean								= false;
		private var _view: View3D								= null;
		private var _cam: Camera3D								= null;
		private var _canvas: Sprite								= null;
		private var _control: HoverController			 		= null;
		private var _zSens: Number								= 1.0;
		private var _xSens: Number								= 2.0;
		private var _ySens: Number								= 2.0;
		private var _moving: Boolean							= false;
		private var _movement: Object							= null;
		private var _listen: Boolean							= true;
		
		public function Camera( parent: Sprite, view: View3D, debug: Boolean = false ) {
			super( parent );
			_debug 					= debug;
			_view					= view;
			_canvas					= parent;
			_cam 					= new Camera3D();
			_cam.project( new Vector3D( 0, 0, 1000 ) );
			_cam.position			= new Vector3D();			
			_cam.lens				= new PerspectiveLens();
			_cam.lens.far			= 1000;
			_control 				= new HoverController( _cam );
			_control.distance		= 80;
			_control.minTiltAngle 	= 0;
			_control.maxTiltAngle 	= 171;
			_control.panAngle 		= 5;
			_control.tiltAngle 		= 350;
			_movement				= new Object();
			_movement.lastPanAngle	= 0;
			_movement.lastTiltAngle	= 0;
			_movement.lastMouseX	= 0;
			_movement.lastMouseY	= 0;
			_movement.minDist		= 60;
			_movement.maxDist		= 150;
			addListeners();
		}
		
		public function get cam3D(): Camera3D {
			return _cam;
		}
		
		public function listen(): void {
			if( _listen ) {
				if( isKeyDown ) {
					switch( lastKey ) {
						case Keyboard.SPACE:
								_cam.position = new Vector3D( 0, 20, -100 );
								break;
						case Keyboard.UP:
							_control.distance -= _zSens;
							break;
							
						case Keyboard.DOWN:
							_control.distance += _zSens;
							break;
					}
				}
			
				if ( _moving ) {
					_control.panAngle = 0.3 * ( _canvas.stage.mouseX - _movement.lastMouseX ) + _movement.lastPanAngle;
					_control.tiltAngle = 0.3 * ( _canvas.stage.mouseY - _movement.lastMouseY ) + _movement.lastTiltAngle;
				}
			
				_control.update();
			}
		}
		
		public function setSensibility( x: Number = 2.0, y: Number = 2.0, z: Number = 1 ): void {
			_xSens = x;
			_ySens = y;
			_zSens = z;
		}
		
		public function get xSens(): Number {
			return _xSens;
		}
		
		public function get ySens(): Number {
			return _ySens;
		}
		
		public function get zSens(): Number {
			return _zSens;
		}
		
		public function set target( obj: ObjectContainer3D ): void {
			_control.lookAtObject = obj;
		}
		
		private function onMouseDown( e: MouseEvent ): void {
			if ( _listen ) {
				_moving = true;
				_movement.lastPanAngle	= _control.panAngle;
				_movement.lastTiltAngle	= _control.tiltAngle;
				_movement.lastMouseX	= _canvas.stage.mouseX;
				_movement.lastMouseY	= _canvas.stage.mouseY;
			}
		}
		
		private function onMouseUp( e: MouseEvent ): void {
			if( _listen )
				_moving = false;
		}
		
		private function onMouseWheel( e: MouseEvent ): void {
			if( _listen ) {
				if ( e.delta != 0 ) {
					var tmp: Number = _control.distance - e.delta;
					
					if ( tmp >= _movement.minDist && tmp <= _movement.maxDist )
						_control.distance -= e.delta;
				}
			}
		}
		
		public function isMoving(): Boolean {
			return _moving;
		}
		
		public function removeListeners(): void {
			_view.removeEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			_view.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			_view.removeEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
		}
		
		public function addListeners(): void {
			_view.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			_view.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			_view.addEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
		}
	
		public function set listening( state: Boolean ): void {
			_listen = state;
		}
	}
	
}
