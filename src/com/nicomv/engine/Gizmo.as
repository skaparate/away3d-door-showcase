package com.nicomv.engine {
	import adobe.utils.CustomActions;
	import away3d.containers.ObjectContainer3D;
	import away3d.containers.Scene3D;
	import away3d.entities.Mesh;
	import away3d.entities.SegmentSet;
	import away3d.materials.SegmentMaterial;
	import away3d.primitives.CubeGeometry;
	import away3d.primitives.LineSegment;
	import away3d.events.MouseEvent3D;
	import away3d.primitives.ConeGeometry;
	import away3d.materials.ColorMaterial;
	import away3d.core.pick.PickingColliderType;
	import away3d.tools.utils.Bounds;
	import away3d.extrusions.LinearExtrude;
	
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
	/**
	 * ...
	 * @author Nicolás Mancilla Vergara
	 */
	public class Gizmo extends ObjectContainer3D {
		private var _tip: Mesh					= null;
		private var _line: LineSegment			= null;
		
		private var _moving: Boolean			= false;
		private var _wireframe: Mesh			= null;
		private var _axis: String				= '';
		private var _half: Vector3D				= null;
		private var _start: Vector3D			= null;
		private var _end: Vector3D				= null;
		
		private static var _count: int		= 0;
		
		public function Gizmo( axis: String = 'x', gizmoName: String = "", mouseUp: Function = null, mouseDown: Function = null ) {
			super();
			_axis			= axis;
			
			if ( gizmoName.length <= 0 )
				name		= gizmoName;
			else
				name		= "gizmo" + _count;
			
			mouseEnabled	= true;
			
			addEventListener( MouseEvent3D.MOUSE_DOWN, function( e: MouseEvent3D ): void {
				onMouseDown( e, mouseDown );
			});
			
			addEventListener( MouseEvent3D.MOUSE_UP, function( e: MouseEvent3D ): void {
				onMouseUp( e, mouseUp );
			});
			
			_count++;
		}
		
		public function init( start: Vector3D, end: Vector3D, color: uint, thick: Number = 3 ): void {
			_half		= new Vector3D();
			_half.x		= start.x + 0.5 * (end.x - start.x);
			_half.y		= start.y + 0.5 * (end.y - start.y);
			_half.z		= start.z + 0.5 * (end.z - start.z);
			
			_line								= new LineSegment( start, end, color, color, thick );
			var segment: SegmentSet				= new SegmentSet();
			segment.addSegment( _line );
			
			var cone: ConeGeometry				= new ConeGeometry( 0.9, 5 );
			_tip								= new Mesh( cone, new ColorMaterial( 0xdedede ) );
			_tip.mouseEnabled					= true;
			_tip.position						= end;
			correctTip();
			addChildren( segment, _tip );
		}
		
		private function onMouseDown( e: MouseEvent3D, cback: Function = null ): void {
			trace( 'Gizmo -> Mouse Down...' );
			_tip.showBounds = true;
			_moving = true;
			
			if ( cback !== null ) {
				cback();
			}
		}
		
		private function onMouseUp( e: MouseEvent3D, cback: Function = null ): void {
			trace( 'Gizmo -> Mouse Up...' );
			_tip.showBounds = false;
			_moving = false;
			
			if ( cback !== null ) {
				cback();
			}
		}
		
		public function update(): void {
			
		}
		
		public function set axis( axis: String ): void {
			_axis = axis;
			correctTip();
		}
		
		public function get axis(): String {
			return _axis;
		}
		
		private function correctTip(): void {
			trace( 'Axis: ' + _axis );
			switch( _axis ) {
				case 'z':
					_tip.rotationX	= 90;
					break;
					
				case 'x':
					_tip.rotationZ	= -90;
					break;
			}
		}
	}

}