﻿package com.nicomv.engine {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	internal class Input {
		private var _lastKey: int = 0;
		private var _isKeyDown: Boolean = false;
		private var _c: Sprite = null;
		/*private var _focused: Boolean = false;
		private var _bounds: Array = new Array(0, 0, 0, 0);*/

		public function Input( canvas: Sprite ) {
			_c = canvas;
			_c.stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			_c.stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
		}
		
		private function onKeyDown( e: KeyboardEvent ): void {
			_lastKey = e.keyCode;
			_isKeyDown = true;
		}
		
		private function onKeyUp( e: KeyboardEvent ): void {
			_isKeyDown = false;
		}
		
		public function get isKeyDown(): Boolean {
			return _isKeyDown;
		}
		
		public function get lastKey(): int {
			return _lastKey;
		}
		
		/*private function onFocus( e: FocusEvent ): void {
			trace('Focused');
			if( ! _canvas.hasEventListener( KeyboardEvent.KEY_DOWN ) ) {
				_canvas.addEventListener( KeyboardEvent.KEY_DOWN, onKey );
			}
		}
		
		private function mouseMove( e: MouseEvent ): void {
			trace(e);
			var x = e.stageX
			var y = e.stageY;
			
			if( ( x > _bounds[0] && x < _bounds[1] ) &&
				  ( y > _bounds[2] && y < _bounds[3] ) &&
					e.buttonDown &&
					! _canvas.hasEventListener( KeyboardEvent.KEY_DOWN ) ) {
				trace( 'SET KEYBOARDEVENT' );
				_canvas.addEventListener( KeyboardEvent.KEY_DOWN, onKey );
			}
		}
		
		private function focusLost( e: FocusEvent ): void {
			trace('Focus lost');
			if( _canvas.hasEventListener( KeyboardEvent.KEY_DOWN ) )
				_canvas.removeEventListener( KeyboardEvent.KEY_DOWN, onKey );
		}*/
	}
	
}
