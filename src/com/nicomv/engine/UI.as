﻿package com.nicomv.engine {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.FrameLabel;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Matrix3D;
	import flash.geom.Point;
	import flash.geom.Vector3D;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	
	// NicoMV imports:
	import com.nicomv.character.Entity;
	import com.nicomv.doors.DoorBase;
	import com.nicomv.engine.Camera;
	import com.nicomv.Utils;
	import com.nicomv.doors.DuraGlide3k;
	import com.nicomv.doors.DuraGlideSLH;
	import com.nicomv.doors.Swing;
	
	// Away 3D imports:
	import away3d.materials.methods.BasicDiffuseMethod;
	import away3d.cameras.Camera3D;
	import away3d.containers.View3D;
	import away3d.containers.Scene3D;
	import away3d.entities.Mesh;
	import away3d.materials.ColorMaterial;
	import away3d.primitives.ConeGeometry;
	import away3d.primitives.CubeGeometry;
	import away3d.primitives.SphereGeometry;
	import away3d.primitives.CylinderGeometry;
	import away3d.core.math.Plane3D;
	import away3d.lights.DirectionalLight;
	import away3d.lights.PointLight;
	import away3d.materials.lightpickers.StaticLightPicker;
	import away3d.primitives.PlaneGeometry;
	import away3d.containers.ObjectContainer3D;
	import away3d.events.MouseEvent3D;
	import away3d.core.base.Geometry;
	import away3d.core.base.SubMesh;
	import away3d.core.base.SubGeometry;
	import away3d.library.AssetLibrary;
	import away3d.events.AssetEvent;
	import away3d.events.LoaderEvent;
	import away3d.loaders.parsers.OBJParser;
	import away3d.loaders.parsers.DAEParser;
	import away3d.loaders.parsers.Max3DSParser;
	import away3d.loaders.parsers.ParserBase;
	import away3d.library.utils.AssetLibraryIterator;
	import away3d.library.assets.IAsset;
	import away3d.debug.AwayStats;
	import away3d.entities.SegmentSet;
	import away3d.primitives.LineSegment;
	import away3d.tools.commands.*;
	import away3d.utils.Cast;
	import away3d.tools.utils.Bounds;
	import away3d.entities.Sprite3D;
	import away3d.filters.tasks.Filter3DDoubleBufferCopyTask;
	import away3d.library.assets.AssetType;
	import away3d.materials.methods.FilteredShadowMapMethod;
	import away3d.materials.TextureMaterial;
	import away3d.textures.BitmapTexture;
	import away3d.bounds.BoundingVolumeBase;
	import away3d.lights.LightProbe;
	
	// Tween library
	import caurina.transitions.Tweener;
	
	public class UI {
		[Embed(source="../../../../embeds/floor.png")]
		private static var FloorTexture: Class;
		
		[Embed(source = "../../../../embeds/ajax-loader.gif")]
		private static var CursorImage: Class;
		
		private var _xml: XML									= null;
		private var _debug: Boolean								= false;
		private var _c: Sprite									= null;
		private var _scene: Scene3D								= null;
		private var _view: View3D								= null;
		private var _cam: Camera								= null;
		private var _controls: Controls							= null;
		private var _dolls: Array								= new Array();
		
		// Models:
		private var _door: DoorBase								= null;
		private var _curModel: Object							= null;
		private var _lastModel: Object							= null;
		private var _lastScale: Object							= null;
		private var _maniqui: Entity							= null;
		private var _curColor: Object							= null;
		
		// Lights:
		private var _lpicker: StaticLightPicker 				= null;
		private var _keyLight: DirectionalLight					= null;
		private var _fillLight: PointLight						= null;
		private var _rimLight: DirectionalLight					= null;
		
		// Floor:
		private var _floorMaterial: TextureMaterial 			= null;
		private var _ground: Mesh								= null;
		
		// Gizmos:
		private var _widthGizmo: Gizmo							= null;
		private var _heightGizmo: Gizmo							= null;
		
		// Height and Width text:
		private var _widthSprite: Sprite3D						= null;
		private var _heightSprite: Sprite3D						= null;
		
		
		public function UI( canvas: Sprite, xml: XML ) {
			_c		= canvas;
			_xml	= xml;
			_debug	= _xml.config.debug == false ? false: true;
		}
		
		public function setup(): void {
			trace( 'SETTING UP' );
			
			// Initializes variables:
			setupView();
			lights();
			
			if( _debug ) {
				_c.addChild( new AwayStats( _view, true, false, 0, true, true ) );
			}
			
			_c.addEventListener( Event.ENTER_FRAME, update );
			_lastScale			= new Object();
			_lastScale.sx 		= 0;
			_lastScale.sy 		= 0;
			_lastScale.sz 		= 0;
			drawScene();
		}
		
		private function setupView(): void {
			trace( 'Setting up view...' );
			_scene		 				= new Scene3D();
			_view						= new View3D();
			_view.scene					= _scene;
			_view.backgroundColor 		= 0x77b5fe;
			_view.antiAlias				= 4;
			_c.addChild( _view );
			_cam						= new Camera( _c, _view, _debug );
			_view.camera				= _cam.cam3D;
		}
		
		private function lights(): void {
			_keyLight				= new DirectionalLight();
			_keyLight.position		= new Vector3D( 3500, 4500, -10000 );
			_keyLight.lookAt( new Vector3D() );
			_keyLight.diffuse 		= 0.7;
			_keyLight.specular		= 0.7;
			_keyLight.color			= 0xfffff0;
			_scene.addChild( _keyLight );

			_fillLight				= new PointLight();
			_fillLight.diffuse		= 0.7;
			_fillLight.specular		= 0.7;
			_fillLight.position		= new Vector3D( 0, 75, 200 );
			_fillLight.color		= 0xffffff;
			_fillLight.radius		= 2000;
			_fillLight.fallOff		= 2000;
			_scene.addChild( _fillLight );
			
			_rimLight				= new DirectionalLight();
			_rimLight.diffuse 		= 0.45;
			_rimLight.specular 		= 0.45;
			_rimLight.position		= new Vector3D();
			_rimLight.color			= 0xfffff0;
			_scene.addChild( _rimLight );
			_lpicker				= new StaticLightPicker( [_keyLight, _fillLight, _rimLight] );
			
			if ( _debug )
				drawLightRays();
		}
		
		private function drawScene(): void {
			drawAxis();	
			addFrontFloor();
			
			_maniqui	= new Entity();
			_maniqui.init();
			
			_controls							= new Controls( _view, _xml.models, _xml.colors );
			_controls.emergencyButton.visible	= false;
			_controls.modelCombo.addEventListener( Event.CHANGE, comboChange );
			_controls.colorCombo.addEventListener( Event.CHANGE, comboChange );
			_controls.changeButton.addEventListener( MouseEvent.CLICK, applyChanges );
			_c.addChild( _controls );
		}
		
		private function addFrontFloor(): void {
			_floorMaterial					= new TextureMaterial( Cast.bitmapTexture( FloorTexture ) );
			//_floorMaterial.shadowMethod 	= new FilteredShadowMapMethod( _keyLight );
			_floorMaterial.lightPicker		= _lpicker;
			_floorMaterial.specular			= 0.3;
			_floorMaterial.diffuseMethod	= new BasicDiffuseMethod();
			_floorMaterial.smooth			= true;
			
			_ground		= new Mesh( new PlaneGeometry( 150, 150 ), _floorMaterial );
			_ground.y = 0;
			_scene.addChild( _ground );
		}
		
		public function set bounds(	dim: Object ): void {
			trace( 'Setting view bounds: ' + [dim.width, dim.height] );
			_view.width				= dim.width;
			_view.height			= dim.height;
			//_controls.updateBounds();
		}
		
		private function update( e: Event ): void {
			var ms: Number = new Date().time / 1000;
			_cam.listen();
			_view.render();
		}
	
		private function applyChanges( e: MouseEvent ): void {
			if( validate() ) {
				trace( 'Applying changes...' );
				_controls.changeButton.removeEventListener( MouseEvent.CLICK, applyChanges );
				_controls.changeButton.label = 'Espere...';
				_controls.openButton.removeEventListener( MouseEvent.CLICK, toggleDoors );
				_controls.emergencyButton.removeEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
			
				var w: Number	= Number( _controls.widthInput.text ) / 100,
					h: Number	= Number( _controls.heightInput.text ) / 100,
					sx:	Number	= w  /  (_curModel.w / 100),
					sy: Number	= h  / (_curModel.h / 100),
					sz: Number	= 1;
					
				setModel( _curModel.file, sx, sy, sz, _curColor );
			}
		}
		
		private function comboChange( e: Event ): void {
			switch( e.target.name ) {
				case 'modelCombo':
					_curModel = Utils.stringToObject( e.currentTarget.selectedItem.data );
					_controls.widthInput.text	= _curModel.w;
					_controls.heightInput.text	= _curModel.h;
					break;
					
				case 'colorCombo':
					if ( e.currentTarget.selectedItem.data.length() <= 0 )
						_curColor	= null;
					else
						_curColor	= Utils.stringToObject( e.currentTarget.selectedItem.data );
					break;
			}
		}
		
		private function toggleDoors( e: MouseEvent = null ): void {
			_controls.openButton.removeEventListener( MouseEvent.CLICK, toggleDoors );
			_controls.emergencyButton.removeEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
			_controls.changeButton.removeEventListener( MouseEvent.CLICK, applyChanges );
			
			if ( _door.open ) {
				removeDolls();
			} else {
				if( _curModel.file !== 'Swing.obj' ) {
					_dolls.push( _maniqui.getModel( true ) );
					_dolls.push( _maniqui.getModel( true ) );
					
					_dolls[0].x += 6;
					_dolls[0].z += 10;
					_dolls[1].x -= 6;
					_dolls[1].z -= 10;
					_dolls[0].rotateTo( 0, 180, 0 );
					
					_scene.addChild( _dolls[0] );
					_scene.addChild( _dolls[1] );
				} else {
					_dolls.push( _maniqui.getModel( true ) );
					_scene.addChild( _dolls[0] );
				}
			}
			
			_door.toggleDoor( null, function(): void {
				if( _door.open ) {
					_controls.openButton.label = 'Cerrar Puerta';
					_controls.openButton.addEventListener( MouseEvent.CLICK, toggleDoors );
				} else {
					_controls.openButton.label = 'Abrir puerta';
					_controls.emergencyButton.addEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
					_controls.openButton.addEventListener( MouseEvent.CLICK, toggleDoors );
					_controls.changeButton.addEventListener( MouseEvent.CLICK, applyChanges );
				}
			});
		}
		
		private function toggleEmergencyOpen( e: MouseEvent = null ): void {
			trace( 'Toggling Emergency Door open...' );
			_controls.openButton.removeEventListener( MouseEvent.CLICK, toggleDoors );
			_controls.emergencyButton.removeEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
			_controls.changeButton.removeEventListener( MouseEvent.CLICK, applyChanges );
			
			if ( _door.open )
				removeDolls();
			
			_door.emergencyToggleDoors( _lastScale, null, function(): void {
				if ( _door.open ) {
					addDolls();
					_controls.emergencyButton.label = 'Cerrar';
					_controls.emergencyButton.addEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
				} else {
					_controls.emergencyButton.label = 'Emergencia';
					_controls.emergencyButton.addEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
					_controls.openButton.addEventListener( MouseEvent.CLICK, toggleDoors );
					_controls.changeButton.addEventListener( MouseEvent.CLICK, applyChanges );
				}
			});
		}
		
		private function setModel( model: String, sx: Number, sy: Number, sz: Number, color: Object ): void {
			trace( 'Set model...' );
			model	= model.replace( /(\.obj|\.3ds)/, '' );
			
			switch( model ) {
				case 'DuraGlide3k':
					trace( '--- DuraGlide3K ---' );
					if ( ! (_door is DuraGlide3k) ) {
						if ( _door !== null )
							_door.dispose();
							
						_door	= new DuraGlide3k( _scene, _lpicker, new Array( sx, sy, sz ), color );
						_door.init();
						_door.addEventListener( DoorBase.DONE, doorReady );
					} else {
						_door.scaleTo( sx, sy, sz );
						_door.changeColor( color );
					}
					
					_controls.emergencyButton.visible	= true;
					break;
					
				case 'DuraGlideSLH':
					trace( '--- DuraGlideSLH ---' );
					if ( ! (_door is DuraGlideSLH) ) {
						if ( _door !== null )
							_door.dispose();
							
						_door	= new DuraGlideSLH( _scene, _lpicker, new Array( sx, sy, sz ), color );
						_door.init();
						_door.addEventListener( DoorBase.DONE, doorReady );
					} else {
						_door.scaleTo( sx, sy, sz );
						_door.changeColor( color );
					}
					
					_controls.emergencyButton.visible	= false;
					break;
					
				case 'Swing':
					trace( '--- Swing ---' );
					
					if ( ! (_door is Swing) ) {
						if ( _door !== null )
							_door.dispose();
							
						_door	= new Swing( _scene, _lpicker, new Array( sx, sy, sz ), color );
						_door.init();
						_door.addEventListener( DoorBase.DONE, doorReady );
					} else {
						_door.scaleTo( sx, sy, sz );
						_door.changeColor( color );
					}
					
					_controls.emergencyButton.visible	= false;
					break;
			}
		}
		
		private function doorReady( e: Event ): void {
			trace( 'Door ready!' );
			if( ! _scene.contains( _door.doors ) )
				_scene.addChild( _door.doors );
				
			if( ! _scene.contains( _door.frame ) )
				_scene.addChild( _door.frame );
				
			drawBoundControls();
		}
		
		private function drawBoundControls(): void {
			trace( 'Drawing Controls...' );
			var frame: Mesh 				= _door.frame,
				start: Vector3D				= new Vector3D(),
				end: Vector3D				= new Vector3D(),
				width: Number				= _door.frameWidth * frame.scaleX,
				height: Number				= _door.frameHeight * frame.scaleY;
				
			trace( 'Frame Width: ' + width  );
			
			start.x	= (frame.x - width / 2) - 5;
			end.x	= start.x;
			end.y	= frame.maxY * frame.scaleY;
			
			var half: Vector3D		= new Vector3D();
			half.x		= start.x + 0.5 * (end.x - start.x);
			half.y		= start.y + 0.5 * (end.y - start.y);
			half.z		= start.z + 0.5 * (end.z - start.z);
			
			if ( _heightSprite !== null )
				_scene.removeChild( _heightSprite );
				
			_heightSprite			= Utils.drawText( _controls.heightInput.text, null, true );
			_heightSprite.position	= half;
			_scene.addChild( _heightSprite );
			
			start = new Vector3D(
				frame.minX * frame.scaleX,
				(frame.y + (frame.maxY * frame.scaleY)) + 5,
				0
			);
			end = new Vector3D(
				frame.maxX * frame.scaleX,
				start.y,
				0
			);
			
			half		= new Vector3D();
			half.x		= start.x + 0.5 * (end.x - start.x);
			half.y		= start.y + 0.5 * (end.y - start.y);
			half.z		= start.z + 0.5 * (end.z - start.z);
			
			if ( _widthSprite !== null )
				_scene.removeChild( _widthSprite );
				
			_widthSprite			= Utils.drawText( _controls.widthInput.text, null, true );
			_widthSprite.position	= half;
			_scene.addChild( _widthSprite );
			
			_controls.changeButton.label	= 'Aplicar';
			_controls.changeButton.addEventListener( MouseEvent.CLICK, applyChanges );
			_controls.openButton.addEventListener( MouseEvent.CLICK, toggleDoors );
			_controls.emergencyButton.addEventListener( MouseEvent.CLICK, toggleEmergencyOpen );
		}
		
		private function addDolls( qty: Number = 10 ): void {
			var	xRange: Array 	= [_door.frame.minX + 6, _door.frame.maxX - 3],
				x: Number		= 0,
				z: Number		= -1;
				
			trace( 'XRange: ' + xRange );
			for ( var i: int = 0; i < qty; i++ ) {
				var tmp: Mesh		= _maniqui.getModel( true ),
					width: Number	= (tmp.maxX - tmp.minX) * 15;
					
				x += width + 1;
				if ( x >= xRange[1]  ) {
					x = xRange[0];
					z += 5;
				}
						
				tmp.x	= x;
				tmp.z	= z;
				_dolls.push( tmp );
				_scene.addChild( tmp );
			}
		}
		
		private function removeDolls(): void {
			_dolls.forEach( function( mesh: Mesh, index: int, array: Array ): void {
				_scene.removeChild( mesh );
				mesh.dispose();
			});
			
			_dolls = null;
			_dolls = new Array();
		}
		
		private function validate(): Boolean {
			var valid: Boolean	= true;
			var w: Number		= Number( _controls.widthInput.text ),
				h: Number		= Number( _controls.heightInput.text );
				
			trace( 'Width: ' + w );
			trace( 'Height: ' + h );
			trace( 'Model: ' + _curModel.file );
			trace( 'Color: ' + _curColor );
			valid &&= h >= 1900 && h <= 3100;
			valid &&= w >= 1000 && w <= 4200;
			valid &&= _curModel.file !== undefined && _curModel.file.length > 0;
			valid &&= _curColor !== null;
			
			trace( 'Valid: ' + valid );
			
			return valid;
		}
	
		private function lookAtObject( e: MouseEvent3D ): void {
			trace( 'Looking at ' + e.target.name );
			_cam.target	= e.target as ObjectContainer3D;
		}
		
		// --- DEBUGGING ---
		private function drawLightRays(): void {
			var thick: Number	= 3,
				seg: SegmentSet	= new SegmentSet();
			
			var line: LineSegment	= new LineSegment(
				_keyLight.position,
				new Vector3D(),
				0xffff00,
				0xffff00,
				thick
			);
			
			seg.addSegment( line );
			
			line					= new LineSegment(
				_fillLight.position,
				new Vector3D(),
				0x0000ff,
				0x0000ff,
				thick
			);
			
			seg.addSegment( line );
			
			line					= new LineSegment(
				_rimLight.position,
				new Vector3D(),
				0x00ff00,
				0x00ff00,
				thick
			);
			
			seg.addSegment( line );
			_scene.addChild( seg );
		}
		
		private function drawAxis(): void {
			if( _debug ) {
				var lset: SegmentSet = new SegmentSet();
				// Positive Axis
				var x: Number			= 0,
						len: Number		= 5,
						thick: Number = 2;
				
				var line: LineSegment = new LineSegment(
					new Vector3D( x, 0, 0 ),
					new Vector3D( x + len, 0, 0 ),
					0xFF0000,
					0xFF0000,
					thick
				);
				lset.addSegment( line );
				
				line = new LineSegment(
					new Vector3D( x, 0, 0 ),
					new Vector3D( x, len, 0 ),
					0x00FF00,
					0x00FF00,
					thick
				);
				lset.addSegment( line );
				
				line = new LineSegment(
					new Vector3D( x, 0, 0 ),
					new Vector3D( x, 0, len ),
					0x0000FF,
					0x0000FF,
					thick
				);
				lset.addSegment( line );
				_scene.addChild( lset );
			}
		}
	}
}
